import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
  useParams
} from 'react-router-dom';

import * as bs from 'bootstrap/dist/css/bootstrap.css';
import Dashboard from './pages/dashboard/Dashboard'
import Login from './pages/login/Login'
import Project from './pages/project/Project'
import BasePage from './pages/BasePage';
import './App.css';
import PrivateRoute from './routerComponents/PrivateRoute';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <PrivateRoute path="/project/:name" children={<ProjectPage/>}/>
          <Route path="/login">
            <BasePage><Login /></BasePage>
          </Route>
          <PrivateRoute path="/">
            <BasePage><Dashboard /></BasePage> 
          </PrivateRoute>
        </Switch>
      </Router>
    </div>
  );
}

function ProjectPage() {
  let {name} = useParams()
  return <BasePage><Project name={name}/></BasePage>
}
export default App;