import getUrl from "./apiUrls";


const login_api = async (username, password) => {
    const response = await fetch(
        getUrl('api/token'),
        {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: username,
                password: password
            })
        }
    );
    const text = await response.text();
    console.log(text)
    if (response.status === 200) {
        return JSON.parse(text);
    } else {
        return null;
    }
}
export default login_api;