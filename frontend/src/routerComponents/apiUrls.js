const deployed = false;

export default function getUrl(apiUrlContinued) {
    if (deployed) {
        return `https://pyvettd-back.herokuapp.com/${apiUrlContinued}`
    }
    else {
        return `http://127.0.0.1:8000/${apiUrlContinued}`;
    }
}