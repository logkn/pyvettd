import React, { useEffect, useState } from 'react'
import Route from 'react-router-dom/Route'
import { Redirect } from 'react-router'

function PrivateRoute ({ children, ...rest }) {


    return (
      <Route {...rest} render={() => {
        return localStorage.getItem("token") != null
          ? children
          : <Redirect to='/login' />
      }} />
    )
  }

export default PrivateRoute
