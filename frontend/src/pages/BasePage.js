import { IconButton, Typography } from '@material-ui/core'
import { Button, Toolbar } from '@material-ui/core'
import HomeIcon from '@material-ui/icons/Home'
import React, { useState } from 'react'
import { Redirect } from 'react-router-dom'

function BasePage({children}) {

    const [toDash, setToDash] = useState(false)

    return (
        <div>
            {toDash && <Redirect to="/"/>}
            <Toolbar>
                <IconButton edge="start" onClick={() => {setToDash(true)}}>
                    <HomeIcon/>
                </IconButton>
                <Typography>PyVettd</Typography>
            </Toolbar>
                
            {children}
        </div>
    )
}

export default BasePage
