import React, { useEffect, useState } from 'react'
import Button from '@material-ui/core/Button'
import { createTheme, Dialog, TextField, FormControl, InputLabel, MenuItem, Select } from '@material-ui/core'
import { DialogTitle } from '@material-ui/core'
import { DialogContent } from '@material-ui/core'
import { Toolbar, IconButton, Typography } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import FunctionCard from './components/FunctionCard'
import InfoIcon from '@material-ui/icons/InfoOutlined'
import EditIcon from '@material-ui/icons/EditOutlined'
import Uploady from '@rpldy/uploady'
import UploadDropZone from "@rpldy/upload-drop-zone";
import download from 'downloadjs/download'
import { Tooltip } from '@material-ui/core'
import SequentialFunctionSelect from './components/SequentialFunctionSelect'
import SequentialCard from './components/SequentialCard'
import FunctionCardLean from './components/FunctionCardLean'
import getUrl from '../../routerComponents/apiUrls'


class Directory {
    constructor(name) {
        this.name = name;
        this.childDirs = {}
        this.files = []
    }

    addFile(filePath, fileObj) {
        filePath = filePath.slice(1)
        const segment = filePath[0];
        if (segment == fileObj.name) {
            this.files.push(fileObj)
        }
        else {
            if (!(segment in this.childDirs )) {
                this.childDirs[segment] = new Directory(segment)
            }
            this.childDirs[segment].addFile(filePath, fileObj)
        }
    }
}


function Project({name}) {

    const [project, setProject] = useState("")

    const [publicFuncs, setPublicFuncs] = useState([])
    const [publicFuncsLoaded, setPublicFuncsLoaded] = useState(false)


    const [projNameEdited, setProjNameEdited] = useState("")
    const [editProjName, setEditProjName] = useState(false)

    const [functionWrapperDialogOpen, setFunctionWrapperDialogOpen] = useState(false)

    const [addPythonDialogOpen, setAddPythonDialogOpen] = useState(false)
    
    const [newPythonFunction, setNewPythonFunction] = useState("")
    const [newFunctionDescription, setNewFunctionDescription] = useState("")
    const [newFunctionTitle, setNewFunctionTitle] = useState("")
    const [newFunctionProcesses, setNewFunctionProcesses] = useState("")
    const [newFunctionFileType, setNewFunctionFileType] = useState("")
    const [newFunctionFileTypeOut, setNewFunctionFileTypeOut] = useState("")

    const [sequentialDialogOpen, setSequentialDialogOpen] = useState(false)

    const [publicFunctionDialogOpen, setPublicFunctionDialogOpen] = useState(false)

    const [requestedFunc, setRequestedFunc] = useState("")
    const [requestedFuncDialogOpen, setRequestedFuncDialogOpen] = useState(false)

    const [openDevInfo, setOpenDevInfo] = useState(false)
    
    const [loading, setLoading] = useState(true)

    const [files, setFiles] = useState([])

    const [updateFuncDialogOpen, setUpdateFuncDialogOpen] = useState(false)

    const [funcToUpdate, setFuncToUpdate] = useState("")

    
    const copyPublic = async (func) => {
        const resp = await fetch(getUrl("api/copy-public"),
            {
                method: "POST",
                body: JSON.stringify({
                    "projectId": project.id,
                    "funcId": func.id,
                })
            }
        )
        updateProject()
        setPublicFunctionDialogOpen(false)
    }

    const updateFuncFromCard = (func) => {
        console.log(func)
        setFuncToUpdate(func)
        setUpdateFuncDialogOpen(true)
    }

    const getPublicFuncs = async () => {
        setPublicFuncsLoaded(false)
        const publicFuncsResp = await fetch(getUrl("api/public-functions"),
            {
                method: "GET"
            }
        )
        const jsonFuncs = await publicFuncsResp.json()
        setPublicFuncs(jsonFuncs[0])
        setPublicFuncsLoaded(true)
    }
    

    const updateFuncClicked = async (functionName) => {
        const resp = await fetch(getUrl("api/update-function"), {
                method: "POST",
                body: JSON.stringify({
                    original_name: functionName,
                    project_name: project.name,
                    function_code: newPythonFunction!="" ? newPythonFunction : funcToUpdate.function,
                    function_desc: newFunctionDescription!="" ? newFunctionDescription : funcToUpdate.desc,
                    function_title: newFunctionTitle!="" ? newFunctionTitle : funcToUpdate.name,
                    processes: newFunctionProcesses!="" ? newFunctionProcesses : funcToUpdate.processes,
                    file_type: newFunctionFileType!="" ? newFunctionFileType : funcToUpdate.file_type,
                    file_type_out: newFunctionFileTypeOut!="" ? newFunctionFileTypeOut : funcToUpdate.file_type_out,
                })
            }
        )
        await updateProject()
        setUpdateFuncDialogOpen(false)
        setFuncToUpdate("")
    }

    const functionEditDialogue = () => {
        const func = funcToUpdate;
        return (
        <Dialog fullScreen open={updateFuncDialogOpen} onClose={() => {setUpdateFuncDialogOpen(false)}} style={{
                
        }}>
            <Toolbar>
                <IconButton edge="start" aria-label="close" onClick={() => {setUpdateFuncDialogOpen(false)}}>
                    <CloseIcon/>
                </IconButton>
                <Tooltip 
                        title="Developer Info"
                    >
                        <IconButton edge="start" aria-label="close" onClick={() => {
                            setUpdateFuncDialogOpen(false)
                            setOpenDevInfo(true)
                        }}>
                        <InfoIcon/>
                    </IconButton>
                </Tooltip>
                <Typography variant="h6">
                    Edit Function
                </Typography>
            </Toolbar>
            <DialogContent>
                <div>
                    <TextField style={{margin: 10}} defaultValue={func.name} label="Function Name" onChange={event => {setNewFunctionTitle(event.target.value)}}/>
                    <TextField style={{margin: 10}} defaultValue={func.desc} fullWidth variant="outlined" rows={3} multiline label="Description" onChange={event => {setNewFunctionDescription(event.target.value)}}/>
                    <div style={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                        
                    }}>
                        <FormControl>
                            <InputLabel>Input File Type</InputLabel>
                            <Select defaultValue={func.file_type} onChange={event => {setNewFunctionFileType(event.target.value)}}>
                                <MenuItem value=".xlsx">.xlsx</MenuItem>
                                <MenuItem value=".csv">.csv</MenuItem>
                                <MenuItem value=".json">.json</MenuItem>
                                <MenuItem value=".txt">.txt</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl>
                            <InputLabel>Output File Type</InputLabel>
                            <Select defaultValue={func.file_type_out} onChange={event => {setNewFunctionFileTypeOut(event.target.value)}}>
                                <MenuItem value=".xlsx">.xlsx</MenuItem>
                                <MenuItem value=".csv">.csv</MenuItem>
                                <MenuItem value=".json">.json</MenuItem>
                                <MenuItem value=".txt">.txt</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl>
                            <InputLabel>Processes</InputLabel>
                            <Select defaultValue={func.processes} onChange={event => {setNewFunctionProcesses(event.target.value)}}>
                                <MenuItem value="File">Single Document</MenuItem>
                                <MenuItem value="Dir">Directory</MenuItem>
                            </Select>
                        </FormControl>
                    </div>
                    
                    <TextField defaultValue={func.function} style={{
                        margin: 10,
                        }} 
                        inputProps={{style: {fontFamily: "monospace"}}}
                        fontFamily="Monospace" fullWidth variant="outlined" rows={6} multiline label="Python Code" onChange={event => {setNewPythonFunction(event.target.value)}}/>
                    </div>
                <Button variant="text" color="default" onClick={() => updateFuncClicked(func.name)}>
                    Update Function
                </Button>
            </DialogContent>
        </Dialog>)
    }

    const crawlDir = async () => {

        const awaited0 = await fetch(getUrl("api/new-upload"), {
                method: "POST",
                body: ""
            }
        )

        const awaited = files.forEach(file => {
            const fd = new FormData();
            fd.append("file", file.file)
            fd.append("path", JSON.stringify(file.path))
            const response = fetch(getUrl("api/upload"), {
                    method: "POST",
                    body: fd
                }
            )
        })
        const awaited2 = await fetch(getUrl("api/upload-done"), {
                method: "POST",
                body: ""
            }
        )
        const text = await awaited2.json()
        console.log(text)

        setRequestedFuncDialogOpen(true)
        
    }

    const runFunctionClicked = async () => {
        const response = await fetch(getUrl("api/run"), {
                method: "POST",
                body: JSON.stringify({
                    "func": requestedFunc,
                    "project": project.name,
                })
            }
        )
        const blob = await response.blob()
        download(blob, `${project.name} (processed).zip`)
    }


    const onFileChange = (f) => {
        const fileVals = Object.values(f);
        const filesForUpload = fileVals.map(file => ({file: file, path: file.webkitRelativePath.split("/")}))
        setFiles(filesForUpload);
    }

    const getFileName = (file) => {
        return file.file.name
    }

    const editProjectName = async () => {

        if (project.name != projNameEdited && projNameEdited != "") {
            const response = await fetch(getUrl("api/update-project-name"), {
                    method: "POST",
                    body: JSON.stringify({
                        "project": project.name,
                        "new": projNameEdited,
                    })
                }
            )
        }
        updateProject()
        setEditProjName(false)
    }

    const editProjTextChange = (value) => {
        if (value.endsWith('\n')) {
            const val = value.trim("\n")
            setProjNameEdited(val)
            editProjName()
        }
        else {
            setProjNameEdited(value)
        }
    }


    const addPythonClicked = () => {
        setAddPythonDialogOpen(true)
    }

    const updateProject = async () => {
        setLoading(true)
        const response = await fetch(getUrl(`api/get-project/${name}`),
            {
                method: "GET",
            }
        );

        const jsonResp = await response.json()
        setProject(jsonResp)

        

        setLoading(false)
    }

    const addSequentialClicked = () => {
        setSequentialDialogOpen(true)
    }

    const createFunctionClicked = async () => {
        const response = await fetch(getUrl("api/create-function"), {
                method: "POST",
                body: JSON.stringify({
                    project_name: project.name,
                    function_code: newPythonFunction,
                    function_desc: newFunctionDescription,
                    function_title: newFunctionTitle,
                    processes: newFunctionProcesses,
                    file_type: newFunctionFileType,
                    file_type_out: newFunctionFileTypeOut,
                })
            }
        )
        updateProject()
        setAddPythonDialogOpen(false)
    }


    useEffect(async () => {
        await updateProject()
    }, [])


    if (loading) {
        return (
            <div>
                Loading...
            </div>
        )
    }

    console.log(publicFuncs)

    return (
        <div>
            {functionEditDialogue()}
            {editProjName ? 
                <TextField autoFocus defaultValue={project.name} onBlur={editProjectName} onChange={event => {editProjTextChange(event.target.value)}}/> :
                <div style={{
                    margin: 'auto',
                    display: 'inline-block'
                }}>
                    <div style={{
                        display: 'flex',
                        flexDirection: 'row'
                    }}>
                        <h1>{project.name}</h1>
                        <span>
                            <IconButton edge="start" onClick={() => {setEditProjName(true)}}>
                                <EditIcon fontSize="small"/>
                            </IconButton>
                        </span>
                    </div>
                </div>
            }
            
            
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-evenly'
            }}>
                
                <div>
                    <Button variant="text" color="default" onClick={() => {setFunctionWrapperDialogOpen(true)}}>
                        Add Function
                    </Button>
                    
                    {project.functions.map(func => {return <FunctionCard func={func} projName={project.name} updateFunc={updateProject} editFunc={updateFuncFromCard}/>})}
                    {project.sequentials.map(seq => (<SequentialCard sequential={seq} project={project.name} updateFunc={updateProject}/>))}
                </div>
                <div style={{
                    display: 'flex',
                    flexDirection: 'column'
                }}>
                    
                    <label htmlFor="btn-upload">
                        <input
                            id="btn-upload"
                            name="btn-upload"
                            style={{ display: 'none' }}
                            type="file"
                            multiple="" directory="" webkitdirectory="" mozdirectory=""
                            onChange={event => {onFileChange(event.target.files)}} />
                        <Button
                            className="btn-choose"
                            variant="outlined"
                            component="span" >
                            Choose Files
                        </Button>
                    </label>
                    {files.length > 0 && 
                    <div>
                        Files:
                        <div style={{
                            display: 'flex',
                            flexDirection: 'column'
                        }}>
                            {files.slice(0, 5).map(file => (<p style={{color:"gray"}}>{getFileName(file)}</p>))}
                            {files.length > 5 && 
                            <p style={{color:'gray'}}>... and {files.length - 5} more.</p>
                            }
                        </div>
                    </div>
                    }
                    <Button disabled={files.length == 0} onClick={crawlDir}>
                        Run
                    </Button>
                </div>
            </div>

            <Dialog fullScreen open={addPythonDialogOpen} onClose={() => {setAddPythonDialogOpen(false)}} style={{
                
            }}>
                <Toolbar>
                    <IconButton edge="start" aria-label="close" onClick={() => {setAddPythonDialogOpen(false)}}>
                        <CloseIcon/>
                    </IconButton>
                    <Tooltip 
                        title="Developer Info"
                    >
                        <IconButton edge="start" aria-label="close" onClick={() => {
                            setAddPythonDialogOpen(false)
                            setOpenDevInfo(true)
                        }}>
                        <InfoIcon/>
                    </IconButton>
                    </Tooltip>
                    
                    <Typography variant="h6">
                        New Python Function
                    </Typography>
                </Toolbar>
                <DialogContent>
                    <div>
                        <TextField style={{margin: 10}} label="Function Name" onChange={event => {setNewFunctionTitle(event.target.value)}}/>
                        <TextField style={{margin: 10}} fullWidth variant="outlined" rows={3} multiline label="Description" onChange={event => {setNewFunctionDescription(event.target.value)}}/>
                        <div style={{
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'space-between',
                            
                        }}>
                            <FormControl>
                                <InputLabel>Input File Type</InputLabel>
                                <Select onChange={event => {setNewFunctionFileType(event.target.value)}}>
                                    <MenuItem value=".xlsx">.xlsx</MenuItem>
                                    <MenuItem value=".csv">.csv</MenuItem>
                                    <MenuItem value=".json">.json</MenuItem>
                                    <MenuItem value=".txt">.txt</MenuItem>
                                </Select>
                            </FormControl>
                            <FormControl>
                                <InputLabel>Output File Type</InputLabel>
                                <Select onChange={event => {setNewFunctionFileTypeOut(event.target.value)}}>
                                    <MenuItem value=".xlsx">.xlsx</MenuItem>
                                    <MenuItem value=".csv">.csv</MenuItem>
                                    <MenuItem value=".json">.json</MenuItem>
                                    <MenuItem value=".txt">.txt</MenuItem>
                                </Select>
                            </FormControl>
                            <FormControl>
                                <InputLabel>Processes</InputLabel>
                                <Select onChange={event => {setNewFunctionProcesses(event.target.value)}}>
                                    <MenuItem value="File">Single Document</MenuItem>
                                    <MenuItem value="Dir">Directory</MenuItem>
                                </Select>
                            </FormControl>
                        </div>
                        
                        <TextField style={{
                            margin: 10,
                            }} 
                            inputProps={{style: {fontFamily: "monospace"}}}
                            fontFamily="Monospace" fullWidth variant="outlined" rows={6} multiline label="Python Code" onChange={event => {setNewPythonFunction(event.target.value)}}/>
                        </div>
                    <Button variant="text" color="default" onClick={createFunctionClicked}>
                        Create Function
                    </Button>
                </DialogContent>
            </Dialog>

            <Dialog open={requestedFuncDialogOpen} onClose={() => {setRequestedFuncDialogOpen(false)}}>
                <Toolbar>
                    <IconButton edge="start" aria-label="close" onClick={() => {setRequestedFuncDialogOpen(false)}}>
                        <CloseIcon/>
                    </IconButton>
                    
                    <Typography variant="h6">
                        Select Function
                    </Typography>
                </Toolbar>
                <FormControl>
                    <InputLabel>Function</InputLabel>
                    <Select onChange={event => {setRequestedFunc(event.target.value)}}>
                        {project.sequentials.map(seq => (
                            <MenuItem value={seq.name}>{seq.name}</MenuItem>
                        ))}
                        {project.functions.map(func => (
                            <MenuItem value={func.name}>{func.name}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <Button onClick={runFunctionClicked}>
                    Go
                </Button>

            </Dialog>

            <Dialog open={openDevInfo} onClose={() => {
                setOpenDevInfo(false)
                if (funcToUpdate == "") {
                    setAddPythonDialogOpen(true)
                }
                else {
                    setUpdateFuncDialogOpen(true)
                }
            }}>
                <Toolbar>
                    <IconButton edge="start" aria-label="close" onClick={() => {
                        setOpenDevInfo(false)
                        if (funcToUpdate == "") {
                            setAddPythonDialogOpen(true)
                        }
                        else {
                            setUpdateFuncDialogOpen(true)
                        }}}>
                        <CloseIcon/>
                    </IconButton>
                    <Typography variant="h6">
                        Developer Info
                    </Typography>
                </Toolbar>
                <DialogContent>
                    <h6>Formatting</h6>
                    <p>The code should be a function that takes a file or directory, depending on what you specify as "Processes".</p>
                    <p>If your argument is a file, the argument type depends on the input file as follows:</p>
                    <ul>
                        <li>JSON: A Python dict</li>
                        <li>XLSX: An openpyxl workbook</li>
                        <li>txt: A string</li>
                        <li>csv: A Pandas DataFrame</li>
                    </ul>

                    <h6>Processing Single Files</h6>
                    <p>Your function will take one argument, and should return a File object.</p>

                    <h6>Processing Directories</h6>
                    <p>Your function will take a single directory argument, and should return a ZIP folder.</p>
                    <p>The directory object will be a nested python dict. Each directory dict (including sub-directories) will have a "dirs" key whose value is a python dict of (path_segment, directory_dict), and a "files" key whose value is a list of the files directly in that directory (not in sub-dirs).</p>
                </DialogContent>
            </Dialog>

            <Dialog open={sequentialDialogOpen} onClose={() => {setSequentialDialogOpen(false)}}>
                <Toolbar>
                    <IconButton edge="start" aria-label="close" onClick={() => {
                        setSequentialDialogOpen(false)}}>
                        <CloseIcon/>
                    </IconButton>
                    <Typography variant="h6">
                        New Sequential
                    </Typography>
                </Toolbar>
                <DialogContent>
                    <SequentialFunctionSelect updateFunc={updateProject} funcs={project.functions} projName={project.name} closeDialogueFunc={() => {setSequentialDialogOpen(false)}}/>
                </DialogContent>
            </Dialog>

            <Dialog open={functionWrapperDialogOpen} onClose={() => {setFunctionWrapperDialogOpen(false)}}>
                <Toolbar>
                    <IconButton edge="start" aria-label="close" onClick={() => {
                        setFunctionWrapperDialogOpen(false)}}>
                        <CloseIcon/>
                    </IconButton>
                    <Typography variant="h6">
                        Add Function
                    </Typography>
                </Toolbar>

                <Button onClick={() => {
                    setFunctionWrapperDialogOpen(false)
                    setAddPythonDialogOpen(true)
                }}>
                    New Python Function
                </Button>
                <Button onClick={() => {
                    setFunctionWrapperDialogOpen(false)
                    setSequentialDialogOpen(true)
                }}>
                    New Sequential
                </Button>
                <Button onClick={() => {
                    setFunctionWrapperDialogOpen(false)
                    getPublicFuncs()
                    setPublicFunctionDialogOpen(true)
                }}>
                    Add from Public
                </Button>
            </Dialog>

            <Dialog fullScreen open={publicFunctionDialogOpen} onClose={() => {setPublicFunctionDialogOpen(false)}}>
                <Toolbar>
                    <IconButton edge="start" aria-label="close" onClick={() => {
                        setPublicFunctionDialogOpen(false)}}>
                        <CloseIcon/>
                    </IconButton>
                    <Typography variant="h6">
                        Public Functions
                    </Typography>
                </Toolbar>

                <div>
                    {!publicFuncsLoaded? <p>Loading...</p>: publicFuncs.map(func => (
                        <Button onClick={() => {copyPublic(func)}} key={func}>
                            <FunctionCardLean func={func} />
                        </Button>
                    ))}
                </div>
                

            </Dialog>
            
        </div>
    )
}

export default Project