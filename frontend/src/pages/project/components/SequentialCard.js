import { Card, CardContent, emphasize, Toolbar, Tooltip } from '@material-ui/core'
import { CardHeader, IconButton } from '@material-ui/core'
import InfoOutlined from '@material-ui/icons/InfoOutlined'
import React, {useState} from 'react'
import DeleteDialog from '../../../universalComponents/DeleteDialog'
import CardTag from './CardTag'
import DeleteIcon from '@material-ui/icons/Delete'
import getUrl from '../../../routerComponents/apiUrls'


const colorMap = {
    ".json": "#FF8F87", // red
    ".txt": "#8DBEF2", // blue
    ".csv": "#FFF694", // yellow
    ".xlsx": "#91EB97", // green
}

function SequentialCard({project, sequential, updateFunc}) {

    const [deleteDialogOpen, setDeleteDialogOpen] = useState(false)

    const startType = () => {
        const funcs = JSON.parse(sequential.funcs)
        return funcs[0].file_type
    }

    const endType = () => {
        const funcs = JSON.parse(sequential.funcs)
        return funcs[funcs.length - 1].file_type_out
    }


    const deleteButtonClicked = async () => {
        const response = await fetch(getUrl("api/sequential"), {
                method: "DELETE",
                body: JSON.stringify({
                    project: project,
                    id: sequential.id
                })
            }
        )
        updateFunc()
    }

    return (
        <>
        <DeleteDialog itemName={sequential.name} open={deleteDialogOpen} setOpen={setDeleteDialogOpen} handleDelete={deleteButtonClicked}/>
        <Card style={{
            margin: 15
        }}>
            <Toolbar>
                <CardHeader title={sequential.name}/>
                <IconButton edge="end" onClick={() => {setDeleteDialogOpen(true)}}>
                    <DeleteIcon fontSize="small"/>
                </IconButton>
            </Toolbar>
            <CardContent>
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        // justifyContent: 'space-evenly'
                    }}
                >
                    {JSON.parse(sequential.funcs).map(func => (<h8>{func.name}</h8>))}
                </div>
            </CardContent>
            
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-evenly',
            }}>
                <div style={{
                    color: 'grey'
                }}>
                    <em>Sequential</em>
                </div>
                <div style={{
                    display: 'flex',
                    flexDirection: 'row'
                }}>
                    <CardTag text={startType()} color={colorMap[startType()]}/>
                    &#8702;
                    <CardTag text={endType()} color={colorMap[endType()]}/>
                </div>
                
            </div>            
        </Card>
        </>
    )
}

export default SequentialCard
