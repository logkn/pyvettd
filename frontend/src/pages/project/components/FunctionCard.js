import { Card, CardActions, CardHeader, Toolbar, IconButton, Tooltip, Menu, MenuItem } from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import InfoIcon from '@material-ui/icons/InfoOutlined'
import EditIcon from '@material-ui/icons/EditOutlined'
import MoreVertIcon from '@material-ui/icons/MoreVert';
import React, { useState } from 'react'
import CardTag from './CardTag'
import DeleteDialog from '../../../universalComponents/DeleteDialog'
import getUrl from '../../../routerComponents/apiUrls';

const colorMap = {
    ".json": "#FF8F87", // red
    ".txt": "#8DBEF2", // blue
    ".csv": "#FFF694", // yellow
    ".xlsx": "#91EB97", // green
}

function FunctionCard({func, projName, updateFunc, editFunc}) {

    const [deleteDialogOpen, setDeleteDialogOpen] = useState(false)
    const [menuOpen, setMenuOpen] = useState(false)
    const [anchorEl, setAnchorEl] = useState(null)

    const editButtonClicked = () => {
        editFunc(func)
    }


    const publish = async () => {
        const response = await fetch(getUrl("api/publish-function"), {
                method: "POST",
                body: JSON.stringify({
                    project_name: projName,
                    function_name: func.name
                })
            }
        )
        updateFunc()
        setMenuOpen(false)
    }

    const deleteButtonClicked = async () => {
        const response = await fetch(getUrl(`api/delete-function`), {
                method: "DELETE",
                body: JSON.stringify({
                    project_name: projName,
                    function_name: func.name
                })
            }
        )
        updateFunc()
    }

    return (
        <>
        <DeleteDialog itemName={func.name} open={deleteDialogOpen} setOpen={setDeleteDialogOpen} handleDelete={deleteButtonClicked}/>
        <Card style={{
            margin: 15
        }}>
            <Toolbar>
                <div>
                    <Tooltip
                        title={
                            <React.Fragment>
                                {func.desc}
                            </React.Fragment>
                        }
                    >
                            <IconButton edge="start">
                                <InfoIcon fontSize="small"/>
                            </IconButton>
                    </Tooltip>
                    <IconButton edge="start" onClick={editButtonClicked}>
                        <EditIcon fontSize="small"/>
                    </IconButton>
                </div>
                <CardHeader title={func.name}/>
                <IconButton edge="end" onClick={() => {setDeleteDialogOpen(true)}}>
                    <DeleteIcon fontSize="small"/>
                </IconButton>
                <IconButton id="more" edge="end" onClick={(event) => {
                    setAnchorEl(event.currentTarget)
                    setMenuOpen(true)

                }}>
                    <MoreVertIcon fontSize="small"/>
                </IconButton>
                <Menu
                    open={menuOpen}
                    onClose={() => {setMenuOpen(false)}}
                    keepMounted
                    anchorEl={anchorEl}

                >
                    <MenuItem onClick={publish}>Make Public</MenuItem>
                </Menu>
            </Toolbar>
            
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-evenly',
            }}>
                <CardTag text={func.processes} color={"white"}/>
                <div style={{
                    display: 'flex',
                    flexDirection: 'row'
                }}>
                    <CardTag text={func.file_type} color={colorMap[func.file_type]}/>
                    &#8702;
                    <CardTag text={func.file_type_out} color={colorMap[func.file_type_out]}/>
                </div>
                
            </div>            
        </Card>
        </>
        
    )
}

export default FunctionCard
