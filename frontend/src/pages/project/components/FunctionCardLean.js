import { Card, CardHeader, Tooltip } from '@material-ui/core'
import { CardContent } from '@material-ui/core'
import { Toolbar } from '@material-ui/core'
import React from 'react'
import CardTag from './CardTag'

const colorMap = {
    ".json": "#FF8F87", // red
    ".txt": "#8DBEF2", // blue
    ".csv": "#FFF694", // yellow
    ".xlsx": "#91EB97", // green
}


function FunctionCardLean({func}) {

    console.log(func)

    return (
        <Card style={{
            margin: 15
        }}>
            <Toolbar>
                <CardHeader title={func.name}/>
            </Toolbar>
            <CardContent>
                {func.desc}
            </CardContent>
            
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-evenly',
            }}>
                <CardTag text={func.processes} color={"white"}/>
                <div style={{
                    display: 'flex',
                    flexDirection: 'row'
                }}>
                    <CardTag text={func.file_type} color={colorMap[func.file_type]}/>
                    &#8702;
                    <CardTag text={func.file_type_out} color={colorMap[func.file_type_out]}/>
                </div>
                
            </div>            
        </Card>
    )
}

export default FunctionCardLean
