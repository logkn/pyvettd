import React from 'react'

function CardTag({text, color}) {
    return (
        <div
            style={{
                backgroundColor: color,
                borderRadius: 3,
                paddingTop: 5,
                paddingInline: 5,
                marginInline: 5,
                marginBottom: 4,
            }}
        >
            <h5 style={{fontSize: 12, fontFamily: 'monospace'}}>{text}</h5>
        </div>
    )
}

export default CardTag
