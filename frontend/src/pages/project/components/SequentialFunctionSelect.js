import { Avatar, Box, Button, IconButton, TextField } from '@material-ui/core'
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUncheckedOutlined'

import React, { useState } from 'react'
import getUrl from '../../../routerComponents/apiUrls'

function SequentialFunctionSelect({funcs, projName, closeDialogueFunc, updateFunc}) {

    const [orderFuncs, setOrderFuncs] = useState([])
    const [name, setName] = useState("")

    const submitSequential = async () => {
        const resp = await fetch(getUrl("api/sequential"), {
                method: "POST",
                body: JSON.stringify({
                    project: projName,
                    name: name,
                    funcs: orderFuncs
                })
            }
        )
        updateFunc()
        closeDialogueFunc()

    }


    const buttonClick = (func2) => {
        if (orderFuncs.includes(func2)) {
            setOrderFuncs(orderFuncs.filter(func => {return func!=func2}))
        }
        else {
            setOrderFuncs(orderFuncs.concat([func2]))
        }
    }

    return (
        <div>
            <TextField label={"Name"} onChange={event => {setName(event.target.value)}}/>
            {funcs.map(func => (
            <SequentialFunctionSingle func={func} orderFuncs={orderFuncs} buttonClick={buttonClick}/>
            ))}
            <Button onClick={submitSequential} disabled={orderFuncs.length < 2}>Create</Button>
        </div>
    )
}


function SequentialFunctionSingle({func, orderFuncs, buttonClick}) {

    if (orderFuncs.includes(func)) {
        return (
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center'
                }}
            >
                <IconButton onClick={() => {buttonClick(func)}}>
                    <Avatar style={{
                        height: '20px',
                        width: '20px',
                        fontSize: 14,
                    }}
                    >
                        {orderFuncs.findIndex(thisFunc=>{return func==thisFunc}) + 1}
                    </Avatar>
                </IconButton>
                <div>
                    {func.name}
                </div>

            </div>
        )
    }

    return (
        <div
        style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center'
        }}
        >
            <IconButton onClick={() => {buttonClick(func)}}>
                <RadioButtonUncheckedIcon fontSize="medium"/>
            </IconButton>
            <div>
                {func.name}
            </div>

        </div>
    )
}


export default SequentialFunctionSelect
