function getDateString(date, useYear) {
    let numToMonth = {
        0: "January",
        1: "February",
        2: "March",
        3: "April",
        4: "May",
        5: "June",
        6: "July",
        7: "August",
        8: "September",
        9: "October",
        10: "November",
        11: "December"
    }

    let dateMonth = date.getMonth()
    let strMonth = numToMonth[dateMonth]
    
    if (useYear) {
        return `${strMonth} ${date.getDay()}, ${date.getFullYear()}`
    }

    else {
        return `${strMonth} ${date.getDay()}`
    }

}

export default function handleDatetime(seconds) {
    var date = new Date(seconds * 1000);
    let dateNow = new Date();    

    let elapsed = (dateNow - date) /1000;

    console.log(elapsed)

    let minutesElapsed = elapsed / 60;

    let hoursElapsed = minutesElapsed / 60;

    let daysElapsed = hoursElapsed / 24;

    if (date.getFullYear() < dateNow.getFullYear()) {
        return getDateString(date, true)
    }

    if (daysElapsed > 7) {
        return getDateString(date, false)
    }

    if (dateNow.getDay() - date.getDay() == 1) {
        return "Yesterday"
    }

    if (dateNow.getDay() - date.getDay() > 1) {
        return `${dateNow.getDay() - date.getDay() } days ago`
    }

    if (Math.floor(hoursElapsed) == 1) {
        return 'About an hour ago'
    }

    if (Math.floor(hoursElapsed) > 1) {
        return `${Math.floor(hoursElapsed)} hours ago`
    }

    if (Math.floor(minutesElapsed) == 1) {
        return 'About a minute ago'
    }

    if (Math.floor(minutesElapsed) > 1) {
        return `${Math.floor(minutesElapsed)} minutes ago`
    }

    return 'A few seconds ago'

}