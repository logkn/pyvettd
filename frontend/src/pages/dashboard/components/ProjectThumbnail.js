import { CardHeader, IconButton, Toolbar } from '@material-ui/core'
import { Button } from '@material-ui/core'
import { Card, CardContent } from '@material-ui/core'
import Delete from '@material-ui/icons/Delete'
import React, { useState } from 'react'
import { Redirect } from 'react-router-dom'
import DeleteDialog from '../../../universalComponents/DeleteDialog'
import handleDatetime from './handleDatetime'
import getUrl from '../../../routerComponents/apiUrls'

function ProjectThumbnail({name, dateModified, id, updateFunc, funcs}) {
    const [redirect, setRedirect] = useState(false)
    const [projDelete, setProjDelete] = useState(false)
    const [deleteDialog, setDeleteDialog] = useState(false)
    
    const cardClick = () => {
        if (!projDelete) {
            setRedirect(true)
        }
    }

    const projectDelete = async () => {
        setProjDelete(true)
        const response = await fetch(getUrl(`api/get-project/${id}`), {
            method: "DELETE",
        })
        updateFunc()
        
    }

    return (
        <>
            {redirect && <Redirect to={`project/${id}`}/>}
            {deleteDialog && <DeleteDialog itemName={name} handleDelete={projectDelete} open={deleteDialog} setOpen={setDeleteDialog}/>}
            <Button onClick={cardClick}>
                <Card >
                    <Toolbar>
                        <IconButton edge="start" onClick={() => setDeleteDialog(true)} onMouseLeave={() => {setProjDelete(false)}} onMouseEnter={() => {setProjDelete(true)}}>
                            <Delete fontSize="small"/>
                        </IconButton>
                        <h5>{name}</h5>
                    </Toolbar>
                    <CardContent>
                        <div style={{
                            display: 'flex',
                            flexDirection: 'column'
                        }}>
                            <pre>Modified: {handleDatetime(dateModified)}</pre>
                            <pre>Functions: {funcs}</pre>
                        </div>
                        
                    </CardContent>
                    
                    
                </Card>
            </Button>
            
        </>
    )
}

export default ProjectThumbnail
