import { Dialog } from '@material-ui/core'
import { TextField } from '@material-ui/core'
import { Button } from '@material-ui/core'
import React, {useEffect, useState} from 'react'
import getUrl from '../../routerComponents/apiUrls'
import DeleteDialog from '../../universalComponents/DeleteDialog'
import ProjectThumbnail from './components/ProjectThumbnail'

function Dashboard() {

    const [projects, setProjects] = useState([])
    const [dialogOpen, setDialogOpen] = useState(false)
    const [newProjectName, setNewProjectName] = useState("")

    const updateProjects = () => {
        fetch(getUrl("api/list-projects"), {
            method: "GET",
        }).then(resp => (resp.json()))
        .then(respJson => {
            const list = respJson.projects
            list.sort((a, b) => {return -a.dateModified + b.dateModified})
            setProjects(list)
        })
    }

    const createProjectClicked = () => {
        setDialogOpen(true)
    }

    const createClicked = () => {
        fetch(getUrl("api/create-project"), {
            method: "POST",
            body: JSON.stringify({name: newProjectName})
        }).then(resp => {
            setDialogOpen(false)
            updateProjects()
        })
    }


    useEffect(() => {
        updateProjects()
    }, [])

    return (
        <div>
            <Button onClick={createProjectClicked}>Create Project</Button>

            <div>
                {projects.map(project => <ProjectThumbnail name={project.name} dateModified={project.dateModified} id={project.id} updateFunc={updateProjects} funcs={project.funcs}/>)}
            </div>


            <Dialog onClose={() => {setDialogOpen(false)}} open={dialogOpen}
                style={{
                    padding: 10
                }}
            >
                <TextField onChange={event => {setNewProjectName(event.target.value)}} label="Project Name"
                    style={{
                        margin: 20
                    }}
                />
                <Button variant="text" color="default"
                    onClick={createClicked}
                >
                    Create
                </Button>
            </Dialog>
        </div>
    )
}

export default Dashboard
