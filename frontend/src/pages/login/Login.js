import React, {useState} from 'react'
import Button from 'react-bootstrap/Button'
import { string } from 'prop-types'
import { Redirect } from 'react-router-dom'
import Alert from 'react-bootstrap/Alert'
import { Input, TextField } from '@material-ui/core'
import login_api from '../../routerComponents/auth_api'
import { textAlign } from '@material-ui/system'
import { useEffect } from 'react'
import getUrl from '../../routerComponents/apiUrls'


function Login() {

    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [loginGood, setLoginGood] = useState(false)
    const [badLogin, setBadLogin] = useState(false)
    

    const buttonClick = async (event) => {
        event.preventDefault();

        const response = await fetch(getUrl("api/make-users"), {
            method: "POST",
        });

        const val = await login_api(username, password)
        if (val === null) {
            setBadLogin(true)
        } else {
            localStorage.setItem("token", val.access)
            setLoginGood(true)
        }
    }

    return (
        <div>
            {loginGood && <Redirect to="/"/>}
            <div style={{
                width: 300,
                margin: 'auto'
            }}>
                <div style={{
                    margin: 10
                }}>
                    <TextField label={"Username"} onChange={event => {setUsername(event.target.value)}}/>
                    <TextField type="password" label={"Password"} onChange={event => {setPassword(event.target.value)}}/>
                </div>
            <Button onClick={event => {buttonClick(event)}}>
                Submit
            </Button>
            {badLogin && <Alert variant="warning">
                Incorrect username and/or password.
            </Alert>}
            
            
            </div>
        </div>
    )
}

export default Login
