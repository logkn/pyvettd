import { Button, DialogContent } from '@material-ui/core'
import { Dialog } from '@material-ui/core'
import React from 'react'

function DeleteDialog({itemName, handleDelete, open, setOpen}) {
    const cancelClicked = () => {
        setOpen(false)
    }

    const deleteClicked = async () => {
        await handleDelete()
        setOpen(false)
    }
    
    return (
        <div>
            {open && <Dialog open={open}>
        <DialogContent>
            Are you sure you want to delete {itemName}?
            <div>
                <Button onClick={cancelClicked}>Cancel</Button>
                <Button color="secondary" onClick={deleteClicked}>Delete</Button>
            </div>
        </DialogContent>
    </Dialog>}
        </div>
        
        
    )
}

export default DeleteDialog
