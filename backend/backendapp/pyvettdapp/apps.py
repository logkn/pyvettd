from django.apps import AppConfig


class PyvettdappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pyvettdapp'
