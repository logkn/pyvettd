# Generated by Django 3.2.5 on 2021-09-16 18:56

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PyVettdFunc',
            fields=[
                ('processes', models.CharField(max_length=15)),
                ('public', models.BooleanField(default=False)),
                ('file_type', models.CharField(max_length=15)),
                ('file_type_out', models.CharField(max_length=15)),
                ('function', models.TextField()),
                ('description', models.TextField()),
                ('name', models.CharField(default='', max_length=50)),
                ('id', models.AutoField(primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='SequentialFunc',
            fields=[
                ('functions', models.TextField()),
                ('name', models.CharField(default='', max_length=50)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('id', models.AutoField(primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('name', models.CharField(default='', max_length=50)),
                ('date_modified', models.DateTimeField(auto_now=True, null=True)),
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('functions', models.ManyToManyField(to='pyvettdapp.PyVettdFunc')),
                ('sequentials', models.ManyToManyField(to='pyvettdapp.SequentialFunc')),
            ],
        ),
    ]
