from django.db import models

# Create your models here.

class PyVettdFunc(models.Model):
    FILE_TYPE_CHOICES = [
        (".xlsx", 'Excel'),
        (".csv", 'CSV'),
        (".txt", 'Text'),
        (".json", 'JSON'),
    ]

    PROCESSES_CHOICES = [
        ("file", "Single File"),
        ("dir", "Directory"),
    ]

    processes = models.CharField(max_length=15)

    public = models.BooleanField(default=False)

    file_type = models.CharField(max_length=15) 
    file_type_out = models.CharField(max_length=15)

    function = models.TextField()
    description = models.TextField()
    name = models.CharField(default="", max_length=50)
    id = models.AutoField(primary_key=True)

    
class SequentialFunc(models.Model):
    functions = models.TextField() # JSON string
    name = models.CharField(default="", max_length=50)
    date_modified = models.DateTimeField( auto_now=True, auto_now_add=False, null = True, blank=True)
    
    id = models.AutoField(primary_key=True)
    

class Project(models.Model):
    functions = models.ManyToManyField(PyVettdFunc)
    sequentials = models.ManyToManyField(SequentialFunc)
    name = models.CharField(default="", max_length=50)
    date_modified = models.DateTimeField( auto_now=True, auto_now_add=False, null = True, blank=True)
    id = models.AutoField(primary_key=True)