from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import authenticate
from django.contrib.auth import login as django_login
from wsgiref.util import FileWrapper
import json
import copy
import django.forms
from django.http import FileResponse
from pyvettdapp.models import Project, PyVettdFunc, SequentialFunc
import datetime as dt
import pandas as pd
import openpyxl
import zipfile
import os
import io
import tempfile
from django.contrib.auth import get_user_model


import re

class FunctionWrapper:
    def __init__(self, function: str ):
        self.func = self.setupFunc(function)
        
    def setupFunc(self, function: str):
        function = function.strip()
        funcName = re.findall("def (.*)\(", function)[0]
        exec(function)
        return eval(funcName)
    
    def run(self, *args):
        return self.func(*args)


class Directory:

    def return_legal_csv_df(self, file, header=0):
        fileOriginal = copy.deepcopy(file)
        try:
            return pd.read_csv(file, header=header)
        except:
            return self.return_legal_csv_df(fileOriginal, header=header+1)

    def handleFile(self, file):
        fname = file.name
        if fname.endswith(".json"):
            return json.load(file)
        if fname.endswith(".csv"):
            return self.return_legal_csv_df(file)
        if fname.endswith(".txt"):
            return file.read()
        if fname.endswith(".xlsx"):
            return openpyxl.open_workbook(file)
        return None
        

    def __init__(self, name="Base"):
        self.allFiles = []
        self.name = name
        self.dirs = {}
        self.files = []

    def addHandled(self, path, handledName, handledObj):
        if handledObj:
            path = path[1:]
            segment = path[0]
            if segment == handledName:
                self.files.append(handledObj)
            else:
                if segment not in self.dirs.keys():
                    newDir = Directory(segment)
                    self.dirs[segment] = newDir
                    self.dirs[segment].addHandled(path, handledName, handledObj)


    def addFile(self, path, fileObj):
        
        handled = self.handleFile(fileObj)
        if handled is not None:
            self.allFiles.append((handled, path))
        path = path[1:]
        segment = path[0]
        if segment == os.path.basename(fileObj.name):
            if handled is not None:
                self.files.append(handled)
        else:
            if segment not in self.dirs.keys():
                newDir = Directory(segment)
                self.dirs[segment] = newDir
            self.dirs[segment].addHandled(path, os.path.basename(fileObj.name), handled)
    
    def reset(self):
        self.name = "Base"
        self.dirs = {}
        self.files = []
        self.allFiles = []

    def printFiles(self):
        for i in self.dirs.values():
            i.printFiles()

    def toDict(self):
        thisDict = {"dirs": {}, "files": [i for i in self.files]}
        thisDict["files"] = [i.to_json() if isinstance(i, pd.DataFrame) else i for i in thisDict["files"]]
        for key, val in self.dirs.items():
            thisDict["dirs"][key] = val.toDict()
        return thisDict
        
    def handleFuncSingle(self, fw: FunctionWrapper, file_type_out: str, returnZip=False):
        s = io.BytesIO()
        zf = zipfile.ZipFile(s, mode='w')
        for file_path in self.allFiles:
            file, path = file_path
            path = [f"{i} (processed)" for i in path]
            path[-1] = path[-1][:-12]
            outFile = fw.run(file)
            fileWithoutExt = os.path.splitext(os.path.join(*path))[0]
            fileWithNewExt = fileWithoutExt + file_type_out
            zf.writestr(fileWithNewExt, outFile)
        if returnZip:
            return zf
        else:
            zf.close()
            return s

    def handleFuncDir(self, fw: FunctionWrapper, file_type_out: str):
        return fw.run(self)

    def _recursiveSplitPath(self, pathStr):
        head, tail = os.path.split(pathStr)
        if head == "":
            if isinstance(tail, str):
                return [tail]
            return tail
        splitHead = self._recursiveSplitPath(head)
        splitHead.append(tail)
        return splitHead

    def reformToZip(self, zf):
        self.reset()
        for i in zf.namelist():
            file = zf.open(i)
            path = self._recursiveSplitPath(i)
            self.addFile(path, file)
        zf.close()

    def handleSequential(self, funcs, stream=None):
        for ind, func in enumerate(funcs):
            fw = FunctionWrapper(func["function"])
            if func["processes"] == "File":
                if ind == len(funcs) - 1: # last function
                    s = self.handleFuncSingle(fw, func["file_type_out"])
                    return s
                else:
                    zf = self.handleFuncSingle(fw, func["file_type_out"], returnZip=True)
                    self.reformToZip(zf)
            else:
                if ind == len(funcs) - 1: # last function
                    s = self.handleFuncDir(fw, func["file_type_out"])
                    return s
                else:
                    zf = self.handleFuncDir(fw, func["file_type_out"])
                    self.reformToZip(zf)
        return None
        




directoryObj = Directory()

def make_users(request):
    if (request.method == "POST"):
        User = get_user_model()
        users = User.objects.all()
        for u in users:
            if u.username == "vettd" :
                return HttpResponse(status = 200)
        
        User.objects.create(username="vettd", password="Pyvettdpass")
        return HttpResponse(status = 200)


# Create your views here.
def getFieldFromRequest(request, field):
    body = request.body.decode("utf-8")
    jsonBody = json.loads(body)
    return jsonBody[field]
    

def listProjects(request):
    if (request.method == "GET"):
        projs = Project.objects.all()
        
        projList = {"projects": [{
            "name": i.name,
            "dateModified": i.date_modified.strftime('%s'),
            "id": str(i.id),
            "funcs": len(list(i.functions.all()))
        } for i in projs]}
        return HttpResponse(json.dumps(projList), status = 200)
    else:
        return HttpResponse(status = 404)

def createProject(request):
    if (request.method == "POST"):
        projName = getFieldFromRequest(request, "name")
        Project.objects.create(name=projName)
        return HttpResponse(status = 200)
    else:
        return HttpResponse(status = 404)

def getPublicFuncs(request):
    if (request.method == "GET"):
        publics = PyVettdFunc.objects.filter(public=True).all()
        funcs = [{"name": i.name, "desc": i.description, "func": i.function, "processes": i.processes, "file_type": i.file_type, "file_type_out": i.file_type_out, "function": i.function, "public": i.public, "id": i.id} for i in list(publics)],
        return JsonResponse(funcs, safe=False)

    else:
        return HttpResponse(status = 404)

def getProject(request, project_id):
    if (request.method == "GET"):
        proj = Project.objects.get(id=project_id)
        projDict = {
            "name": proj.name,
            "id": proj.id,
            "dateModified": proj.date_modified.strftime('%s'),
            "functions": [{"name": i.name, "desc": i.description, "func": i.function, "processes": i.processes, "file_type": i.file_type, "file_type_out": i.file_type_out, "function": i.function, "public": i.public, "id":i.id} for i in list(proj.functions.all())],
            "sequentials": [{"name": i.name, "funcs": i.functions, "dateModified": i.date_modified.strftime('%s'), "id": i.id} for i in list(proj.sequentials.all())]
        }
        return JsonResponse(projDict)

    if (request.method == "DELETE"):
        proj = Project.objects.get(id=project_id)
        proj.delete()
        return HttpResponse(status = 200)

    else:
        return HttpResponse(status = 404)

def newFunction(request):
    if (request.method == "POST"):
        projName = getFieldFromRequest(request, "project_name")
        funcName = getFieldFromRequest(request, "function_title")
        funcDesc = getFieldFromRequest(request, "function_desc")
        funcCode = getFieldFromRequest(request, "function_code")
        fileType = getFieldFromRequest(request, "file_type")
        fileTypeOut = getFieldFromRequest(request, "file_type_out")
        processes = getFieldFromRequest(request, "processes")

        func = PyVettdFunc(function=funcCode, description=funcDesc, name=funcName, file_type=fileType, processes=processes, file_type_out=fileTypeOut)
        func.save()

        proj = Project.objects.get(name=projName)
        proj.functions.add(func)
        proj.save()

        return HttpResponse(status = 200)
    else:
        return HttpResponse(status = 404)

def publishFunction(request):
    if request.method == "POST":
        projname = getFieldFromRequest(request, "project_name")
        funcname = getFieldFromRequest(request, "function_name")
        proj = Project.objects.get(name=projname)
        func = proj.functions.get(name=funcname)
        func.public = True
        func.save()
        proj.save()
        return HttpResponse(status = 200)

def copyPublic(request):
    if request.method == "POST":
        projid = getFieldFromRequest(request, "projectId")
        funcid = getFieldFromRequest(request, "funcId")
        proj = Project.objects.get(id = projid)
        func = PyVettdFunc.objects.get(id=funcid)
        func.id = None
        func.save()
        proj.functions.add(func)
        proj.save()
        return HttpResponse(status = 200)
    else:
        return HttpResponse(status = 404)

def sequential(request):
    if request.method == "POST":
        name = getFieldFromRequest(request, "name")
        projname = getFieldFromRequest(request, "project")
        funcs = getFieldFromRequest(request, "funcs")

        proj = Project.objects.get(name = projname)
        sequential = SequentialFunc(functions = json.dumps(funcs), name=name)
        sequential.save()
        proj.sequentials.add(sequential)
        proj.save()
        return HttpResponse(status = 200)

    if request.method == "DELETE":
        id = getFieldFromRequest(request, "id")
        projname = getFieldFromRequest(request, "project")

        proj = Project.objects.get(name = projname)
        seq = proj.sequentials.get(id=id)
        seq.delete()
        proj.save()
        return HttpResponse(status=200)



    else:
        return HttpResponse(status = 404)

def updateFunction(request):
    if (request.method == "POST"):
        projName = getFieldFromRequest(request, "project_name")
        funcName = getFieldFromRequest(request, "function_title")
        funcDesc = getFieldFromRequest(request, "function_desc")
        funcCode = getFieldFromRequest(request, "function_code")
        fileType = getFieldFromRequest(request, "file_type")
        fileTypeOut = getFieldFromRequest(request, "file_type_out")
        processes = getFieldFromRequest(request, "processes")

        originalName = getFieldFromRequest(request, "original_name")

        proj = Project.objects.get(name=projName)
        func = proj.functions.get(name=originalName)

        func.name = funcName
        func.description = funcDesc
        func.function = funcCode
        func.file_type = fileType
        func.file_type_out = fileTypeOut
        func.processes = processes

        func.save()
        return HttpResponse(status = 200)

    else:
        return HttpResponse(status = 404)

def deleteFunction(request):
    projName = getFieldFromRequest(request, "project_name")
    funcName = getFieldFromRequest(request, "function_name")

    proj = Project.objects.get(name=projName)

    func = proj.functions.get(name=funcName)

    func.delete()
    return HttpResponse(status=200)

def updateProj(request):
    if (request.method == "POST"):
        original = getFieldFromRequest(request, "project")
        new = getFieldFromRequest(request, "new")

        proj = Project.objects.get(name = original)
        proj.name = new
        proj.save()
        return HttpResponse(status = 200)
    else:
        return HttpResponse(status = 404)

def uploadFiles(request):
    if (request.method == "POST"):
        file = request.FILES["file"]
        path = json.loads(request.POST["path"])
        directoryObj.addFile(path, file)
        return HttpResponse(status=200)
    else:
        return HttpResponse(status = 404)

def filesUploaded(request):
    directoryObj.printFiles()
    jsonDir = json.dumps(directoryObj.toDict())
    return HttpResponse(jsonDir, status=200)

def newUpload(request):
    directoryObj.reset()
    return HttpResponse(status=200)

def applySingleFileFunction(request):
    func = getFieldFromRequest(request, "func")


def run(request):
        
    if request.method == "POST":
        funcName = getFieldFromRequest(request, "func")
        projName = getFieldFromRequest(request, "project")
        projObj = Project.objects.get(name=projName)
        try:
            funcObj = projObj.functions.get(name=funcName)
            if funcObj.processes == "File":
                functionWrapper = FunctionWrapper(funcObj.function)
                response_zip = directoryObj.handleFuncSingle(functionWrapper, funcObj.file_type_out)
            else:
                response_zip = directoryObj.handleFuncDir(functionWrapper, funcObj.file_type_out)
        
        except:
            sequential = projObj.sequentials.get(name=funcName)
            response_zip = directoryObj.handleSequential(json.loads(sequential.functions))

        
        resp = HttpResponse(response_zip.getvalue(), content_type="application/zip")
        zipOutName = "out.zip"
        resp["Content-Disposition"] = 'attachment; filename=%s'%zipOutName
        return resp
    else:
        return HttpResponse(status = 404)
