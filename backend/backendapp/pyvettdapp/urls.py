from django.urls import path
from . import views
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = [
    path('token', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh', TokenRefreshView.as_view(), name = "token_refresh"),
    path('list-projects', views.listProjects),
    path('create-project', views.createProject),
    path('get-project/<int:project_id>', views.getProject),
    path('create-function', views.newFunction),
    path('update-project-name', views.updateProj),
    path('update-function', views.updateFunction),
    path('copy-public', views.copyPublic),
    path('delete-function', views.deleteFunction),
    path('publish-function', views.publishFunction),
    path('public-functions', views.getPublicFuncs),
    path('upload', views.uploadFiles),
    path('sequential', views.sequential),
    path('upload-done', views.filesUploaded),
    path('new-upload', views.newUpload),
    path('run', views.run),
    path('make-users', views.make_users),
]